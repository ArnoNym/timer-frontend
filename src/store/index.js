import Vue from 'vue';
import Vuex from 'vuex';
import access from "./modules/access";
import actors from "./modules/actors";
import socket from "./modules/socket";
import liveUpdate from "./modules/liveUpdate";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    access,
    actors,
    socket,
    liveUpdate
  },
  state: {
  },
  mutations: {
  },
  actions: {
  }
});
