import {SubscribeTypes} from "@/constants/types/subscribe-types";
import SubscribeInfo from "@/classes/subscribe-info";
import {MutationTypes} from "@/constants/types/mutation-types";

export const generateSubscribeInfos = (rootGetters, commit, dispatch) => [
    new SubscribeInfo(SubscribeTypes.ADD_ACTOR, message => {
            console.log("CALLED SubscribeTypes.ADD_ACTOR");
            const messageBody = JSON.parse(message.body);
            commit(MutationTypes.ADD_ACTOR, messageBody);
        }
    ),
    new SubscribeInfo(SubscribeTypes.REMOVE_ACTOR, message => {
            console.log("CALLED SubscribeTypes.REMOVE_ACTOR");
            const actorId = message.body;
            dispatch("removeActor", actorId);
        }
    ),
    new SubscribeInfo(SubscribeTypes.ADD_START, message => {
            console.log("CALLED SubscribeTypes.ADD_START");
            const actorIdEndedActActWrapper = JSON.parse(message.body);
            const actorId = actorIdEndedActActWrapper.actorId;
            const act = actorIdEndedActActWrapper.act;
            const endedAct = actorIdEndedActActWrapper.endedAct;
            if (endedAct != null) {
                if (rootGetters.synchronizedForAddEnd) {
                    commit(MutationTypes.ADD_END, {
                        actorId: null,
                        act: endedAct
                    });
                } else {
                    dispatch("syncActors");
                    return;
                }
            }
            commit(MutationTypes.ADD_START, {
                actorId: actorId,
                act: act
            });
            commit(MutationTypes.SET_CURRENT_ACTOR_BY_ID, actorId);
            dispatch("startLiveUpdate");
        }
    ),
    new SubscribeInfo(SubscribeTypes.ADD_END, message => {
            console.log("CALLED SubscribeTypes.ADD_END");
            const act = JSON.parse(message.body);
            if (rootGetters.synchronizedForAddEnd) {
                commit(MutationTypes.ADD_END, {
                    actorId: rootGetters.currentActorId,
                    act: act
                });
                commit(MutationTypes.RESET_CURRENT_ACTOR);
                dispatch("stopLiveUpdate");
            } else {
                dispatch("syncActors");
            }
        }
    ),
]
