import {MutationTypes} from "@/constants/types/mutation-types";
import {LIVE_UPDATE_PAUSE_MS} from "@/constants/constants";

const state = {
    currentIntervalId: null
};

const getters = {
    live: state => state.currentIntervalId != null,
    currentIntervalId: state => state.currentIntervalId,
};

const mutations = {
    [MutationTypes.SET_CURRENT_LIVE_UPDATE_INTERVAL_ID]: (state, currentIntervalId) => {
        state.currentIntervalId = currentIntervalId;
    },
};

const actions = {
    async startLiveUpdate({getters, commit}) {
        if (getters.live) {
            return;
        }

        const handler = () => {
            commit(MutationTypes.UPDATE_CURRENT_ACTOR_PASSED_TIME);
        }

        const intervalId = window.setInterval(handler, LIVE_UPDATE_PAUSE_MS);

        commit(MutationTypes.SET_CURRENT_LIVE_UPDATE_INTERVAL_ID, intervalId);
    },
    async stopLiveUpdate({getters, commit}) {
        window.clearInterval(getters.currentIntervalId);
        commit(MutationTypes.SET_CURRENT_LIVE_UPDATE_INTERVAL_ID, null);
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};

