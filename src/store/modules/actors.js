import {MutationTypes} from "@/constants/types/mutation-types";
import {MessageTypes} from "@/constants/types/message-types";
import Actor from "@/classes/actor";
import {FetchTypes} from "@/constants/types/fetch-types";

const state = {
    actors: [],
    currentActor: null,
    passedTime: 0 // Only for efficiency
};

const getters = {
    actors: state => state.actors,

    stopped: state => state.currentActor === null,

    currentActorId: state => state.currentActor == null ? null : state.currentActor.id,
    currentActor: state => state.currentActor,
    currentAct: state => state.currentActor === null ? null : state.currentActor.acts[state.currentActor.acts.length - 1],

    passedTime: state => state.passedTime,
    actorPassedTime: state => actorId => {
        for (let actor of state.actors) {
            if (actor.id === actorId) {
                return actor.passedTime;
            }
        }
    }
};

const mutations = {
    [MutationTypes.SET_ACTORS]: (state, actors) => {
        state.actors = actors;
    },
    [MutationTypes.ADD_ACTOR]: (state, actor) => {
        console.log("MutationTypes.ADD_ACTOR")
        console.log(state.actors)
        console.log(actor)
        actor = new Actor(actor.id, actor.name, actor.acts);
        state.actors.push(actor);
        console.log("MutationTypes.ADD_ACTOR after push")
        console.log(state.actors)
        console.log(actor)
        //"ä".localeCompare("b", "de-DE")
        //arr.splice(index, 0, item); // todo auch vor ort slphabetisch sortieren. Aber genau so wie der server ...
    },
    [MutationTypes.REMOVE_ACTOR]: (state, actorId) => {
        state.actors = state.actors.filter(actor => actor.id !== actorId);
    },
    [MutationTypes.ADD_START]: (state, {actorId, act}) => {
        for (let actor of state.actors) {
            if (actor.id === actorId) {
                actor.acts.push(act);
                return;
            }
        }
        console.error("Could not find actor with id="+actorId+"!");
    },
    [MutationTypes.ADD_END]: (state, {actorId = null, act}) => {
        console.log("CALLED MutationTypes.ADD_END")
        let actorsAct;
        if (actorId == null) {
            for (let actor of state.actors) {
                if (actor.acts.length > 0) {
                    actorsAct = actor.acts[actor.acts.length - 1]
                    if (actorsAct.id === act.id) {
                        break;
                    }
                }
            }
            if (actorsAct == null) {
                console.error("Could not find act with id= "+act.id+" !");
                return;
            }
        } else {
            for (let actor of state.actors) {
                if (actor.id === actorId) {
                    actorsAct = actor.acts[actor.acts.length - 1];
                    break;
                }
            }
        }
        console.log("actorsAct.id="+actorsAct.id+", act.id="+act.id+", actorsAct.start="+actorsAct.start+", act.start="+act.start+", actorsAct.end="+actorsAct.end)
        if (state.currentActor != null && actorsAct.id === act.id && actorsAct.start === act.start && actorsAct.end === null) {
            actorsAct.end = act.end;
            state.passedTime += state.currentActor.updatePassedTime();
        } else {
            console.error("Out of sync!");
        }
    },
    [MutationTypes.RESET_CURRENT_ACTOR]: (state) => {
        state.currentActor = null;
    },
    [MutationTypes.SET_CURRENT_ACTOR]: (state, actor) => {
        state.currentActor = actor;
    },
    [MutationTypes.SET_CURRENT_ACTOR_BY_ID]: (state, actorId) => {
        let actor;
        for (actor of state.actors) {
            if (actor.id === actorId) {
                break;
            }
        }
        if (actor === null) {
            console.error("Could not find actor with id=" + actorId);
        }
        state.currentActor = actor;
    },
    [MutationTypes.UPDATE_CURRENT_ACTOR_PASSED_TIME]: (state) => {
        const diff = state.currentActor.updateRunningPassedTime();
        state.passedTime += diff;
    },
    [MutationTypes.SET_PASSED_TIME]: (state, passedTime) => {
        state.passedTime = passedTime;
    },
};

const actions = {
    async removeActor({dispatch, commit, getters}, actorId) {
        let actor;
        const currentActor = getters.currentActor;
        if (currentActor != null && actorId === currentActor.id) {
            actor = currentActor;
            await dispatch("stopLiveUpdate");
            commit(MutationTypes.RESET_CURRENT_ACTOR);
        } else {
            for (actor of state.actors) {
                if (actor.id === actorId) {
                    break;
                }
            }
        }
        commit(MutationTypes.SET_PASSED_TIME, getters.passedTime - actor.passedTime);
        commit(MutationTypes.REMOVE_ACTOR, actorId);
    },

    // Using the socket

    async syncActors({dispatch, commit, rootGetters}) { // Called in access.js. Dont know why its not linked by webstorm
        console.log("START syncActors");
        const response = await fetch(FetchTypes.GET_ACTORS, rootGetters.createDataByStringRequest(rootGetters.accountId));
        if (response.ok) {
            const actors = await response.json();

            const actorObjects = [];
            let currentActor = null;
            let passedTime = 0;

            for (let actor of actors) {
                const actorObject = new Actor(actor.id, actor.name, actor.acts);

                actorObjects.push(actorObject);

                if (actorObject.isCurrentActor()) {
                    if (currentActor != null) {
                        console.error("Fetched actors seem to be compromised!")
                    }
                    currentActor = actorObject;
                }

                passedTime += actorObject.passedTime;
            }

            commit(MutationTypes.SET_ACTORS, actorObjects);
            if (currentActor != null) {
                commit(MutationTypes.SET_CURRENT_ACTOR, currentActor);
                dispatch("startLiveUpdate");
            }
            commit(MutationTypes.SET_PASSED_TIME, passedTime);
        } else {
            console.error("Could not fetch actors from database!");
        }
        console.log("END syncActors");
    },

    async newActor({dispatch, rootGetters}, actorName) {
        await dispatch("send", {
            messageType: MessageTypes.NEW_ACTOR,
            message: {
                accountId: rootGetters.accountId,
                actorName: actorName
            },
            messageIsObject: true
        });
    },
    async deleteActor({dispatch, rootGetters}, actorId) {
        await dispatch("send", {
            messageType: MessageTypes.DELETE_ACTOR,
            message: {
                accountId: rootGetters.accountId,
                actorId: actorId
            },
            messageIsObject: true
        });
    },
    async start({dispatch, rootGetters}, startActorId) {
        /*
        if (rootGetters.currentAct != null) {
            await dispatch("stop");
        }
        await waitFor(() => getters.stopped) // todo bessere loesung??
        if (rootGetters.currentAct != null) {
            console.error("Server connection issues!"); //TODO
        }
        */
        await dispatch("send", {
            messageType: MessageTypes.NEW_START,
            message: {
                accountId: rootGetters.accountId,
                actorId: startActorId
            },
            messageIsObject: true
        });
    },
    async stop({dispatch, getters, rootGetters}) { // Is in use. No idea why its not marked
        console.log("Current actor is about to stop!");
        if (getters.stopped) {
            console.log("There is nothing to stop!");
            return;
        }
        await dispatch("send", {
            messageType: MessageTypes.NEW_END,
            message: {
                accountId: rootGetters.accountId,
                actId: rootGetters.currentAct.id
            },
            messageIsObject: true
        });
        console.log("Current actor stopped!")
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};
