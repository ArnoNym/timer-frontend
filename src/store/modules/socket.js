import {MutationTypes} from "@/constants/types/mutation-types";
import SockJS from "sockjs-client";
import Stomp from "webstomp-client";
import {DATABASE_WEBSOCKET_URL} from "@/constants/urls";
import {createDestination} from "@/utils/utils";
import {generateSubscribeInfos} from "@/store/addons/subscriptions";

const state = {
    stompClient: null,
    connected: false
};

const getters = {
    stompClient: state => state.stompClient,
    connected: state => state.connected,
    createDestination: (state, rootGetters) => subscribeType => createDestination(subscribeType, rootGetters.accountId),
    synchronizedForAddEnd: state => (stateCurrentAct, databaseCurrentAct) => state.connected
        && state.currentActor != null
        && stateCurrentAct.id === databaseCurrentAct.id
        && stateCurrentAct.start === databaseCurrentAct.start
        && stateCurrentAct.end === null //TODO Das wird nirgendwo wirklich sinnvoll verwendet. Echte synchonitaet wird leider nei moeglich sein. Vielleicht besser einen Refresh knopf
};

const mutations = {
    [MutationTypes.SET_STOMP_CLIENT]: (state, stompClient) => {
        state.stompClient = stompClient;
    },
    [MutationTypes.SET_CONNECTED]: (state, connected) => {
        state.connected = connected;
    },
};

const actions = {
    async connect({commit, dispatch, rootGetters}) {
        console.log("START connect");

        await dispatch("calcAccount");
        console.log("Account calculated!");

        const socket = await new SockJS(DATABASE_WEBSOCKET_URL);
        const stompClient = await Stomp.over(socket);
        await stompClient.connect(
            {
                // Das ist nicht der uebliche http header sondern ein in der stomp connection versteckter
            },
            frame => {
                console.log(frame);

                commit(MutationTypes.SET_CONNECTED, true);
                commit(MutationTypes.SET_STOMP_CLIENT, stompClient);

                for (let subscribeInfo of generateSubscribeInfos(rootGetters, commit, dispatch)) {
                    stompClient.subscribe(rootGetters.createDestination(subscribeInfo.type), subscribeInfo.messageHandler);
                }
            },
            error => {
                console.log(error);
                commit(MutationTypes.SET_CONNECTED, false);
            }
        );
        console.log("Connected via socket!");

        console.log("END connect");
    },
    async disconnect({commit, getters}) {
        if (getters.stompClient) {
            getters.stompClient.disconnect();
        }
        commit(MutationTypes.SET_CONNECTED, false);
    },
    async toggleConnection({getters, dispatch}) {
        getters.connected ? dispatch.disconnect() : dispatch.connect();
    },
    async send({getters}, {messageType, message, messageIsObject}) {
        if (getters.stompClient && getters.stompClient.connected) {
            console.log(JSON.stringify(message));
            getters.stompClient.send("/app"+messageType, messageIsObject ? JSON.stringify(message) : message, {});
        } else {
            console.log("Could not send message via stomp! this.stompClient="+getters.stompClient+", connected="+getters.stompClient.connected);
        }
    },
    /*
    async subscribe({getters}, channel) {
        sendMe
        //Subscribe to the channel
        getters.connection.send(JSON.stringify({
            command: "subscribe",
            identifier: "{\"channel\":\"" + channel + "\"}"
        }));
    }
    */
};

export default {
    state,
    getters,
    actions,
    mutations
};
