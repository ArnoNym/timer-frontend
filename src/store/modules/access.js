import {MutationTypes} from "@/constants/types/mutation-types";
import {FetchTypes} from "@/constants/types/fetch-types";
import router from "@/router";

const state = {
    account: null,
};

const getters = {
    account: state => state.account,
    accountId: state => state.account === null ? null : state.account.id,

    createBody: () => data => {
        return JSON.stringify(data);
    },
    createEmptyRequest: (state, getters) => () => {
        return getters.createDataByStringRequest();
    },
    createDataByObjectRequest: (state, getters) => (object) => {
        return getters.createRequest(true, true, true, object);
    },
    createDataByStringRequest: (state, getters) => (string) => {
        return getters.createRequest(true, true, false, string);
    },
    createRequest: (state, getters) => (
        postBoolean, // if false get. Post bedeutet es wird nicht ind er URL angezeigt. Get wird angezeigt
        noCorsBoolean, // if false corse. Wenn corse sind Zugriffe von anderen URLs aus verboten localhost:8080 darf dann nur auf localhost:8080 zugreifen
        objectBoolean, // if false content ist expected to be a string
        content
    ) => {
        const request = {
            method: postBoolean ? "POST" : "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: objectBoolean ? getters.createBody(content) : content
        }
        if (noCorsBoolean) {
            //request["mode"] = "no-cors";//was zum fick geht ab. Scheinbar wurde das nie verwendet
        }
        if (getters.accountId != null) {
            request["headers"]["Authorization"] = getters.accountId;
        }
        console.log("request: ")
        console.log(request)
        return request;
    }
};

const mutations = {
    [MutationTypes.SET_ACCOUNT]: (state, account) => {
        console.log("Set account = ");
        console.log(account);
        state.account = account;
    },
    [MutationTypes.SET_ACCOUNT_ID_IN_ROUTE]: (state, accountId) => {
        router.replace({name: "Home", params: {accountId: accountId}, query: {}}).then(() => {});
    },
};

const actions = {
    async calcAccount({dispatch, commit, getters}) {
        const storeAccountId = getters.accountId;
        const routeAccountId = router.currentRoute.params.accountId;

        if (storeAccountId != null && storeAccountId === routeAccountId) {
            return;
        }

        if (routeAccountId == null) {
            // Get new Account from server
            const response = await fetch(FetchTypes.CREATE_ACCOUNT, getters.createEmptyRequest());
            if(response.ok) {
                const account = await response.json();
                commit(MutationTypes.SET_ACCOUNT, account);
                commit(MutationTypes.SET_ACCOUNT_ID_IN_ROUTE, account.id);
            } else {
                console.error("Could not create account!");
            }
        } else {
            // Get Account from url and check with the server if it exists
            const response = await fetch(FetchTypes.GET_ACCOUNT, getters.createDataByStringRequest(routeAccountId));
            if (response.ok) {
                const account = await response.json();
                commit(MutationTypes.SET_ACCOUNT, account);
                await dispatch("syncActors");
            } else {
                alert("URL not found!");
            }
        }
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};
