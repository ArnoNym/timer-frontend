export const DEPLOYED = 2; /*
    0 = local aus intellij heraus
    1 = local auf tomcat
    2 = server auf tomcat
*/

export const HASH_ROUTING = true;

export const APP_FRONTEND_NAME = "timer";
export const APP_BACKEND_NAME = "timer-backend";

export const LIVE_UPDATE_PAUSE_MS = 1000;


