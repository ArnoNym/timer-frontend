import {DEPLOYED, APP_BACKEND_NAME, APP_FRONTEND_NAME, HASH_ROUTING} from "@/constants/constants";

export const DATABASE_URL = function() {
    let url = "";
    switch(DEPLOYED) {
        case 0:
        case 1:
            url += "http://localhost:8080";
            break;
        case 2:
            url += "http://85.214.169.195:8080";
            break;
    }
    switch(DEPLOYED) {
        case 0:
            break;
        case 1:
        case 2:
            url += "/" + APP_BACKEND_NAME;
            break;
    }
    return url;
}();

export const FRONTEND_URL = function() {
    let url = "";
    switch(DEPLOYED) {
        case 0:
            url += "http://localhost:8081";
            break;
        case 1:
            url += "http://localhost:8080";
            break;
        case 2:
            url += "http://85.214.169.195:8080";
            break;
    }
    switch(DEPLOYED) {
        case 0:
            break;
        case 1:
        case 2:
            url += "/" + APP_FRONTEND_NAME;
            break;
    }
    if(HASH_ROUTING) {
        url += "/#"
    }
    return url;
}();

export const DATABASE_WEBSOCKET_URL = DATABASE_URL + "/socket";
