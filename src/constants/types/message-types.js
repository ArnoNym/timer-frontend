export const MessageTypes = {
    NEW_ACTOR: "/newActor",
    DELETE_ACTOR: "/deleteActor",
    NEW_START: "/newStart",
    NEW_END: "/newEnd",
}
