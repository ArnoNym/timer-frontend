export const MutationTypes = {
    SET_CURRENT_LIVE_UPDATE_INTERVAL_ID: "SET_CURRENT_LIVE_UPDATE_INTERVAL_ID",

    SET_STOMP_CLIENT: "SET_STOMP_CLIENT",
    SET_CONNECTED: "SET_CONNECTED",

    SET_ACCOUNT: "SET_ACCOUNT",
    SET_ACCOUNT_ID_IN_ROUTE: "SET_ACCOUNT_ID_IN_ROUTE",

    SET_ACTORS: "SET_ACTORS",
    ADD_ACTOR: "ADD_ACTOR",
    REMOVE_ACTOR: "REMOVE_ACTOR",
    ADD_START: "ADD_START",
    ADD_END: "ADD_END",

    SET_CURRENT_ACTOR: "SET_CURRENT_ACTOR",
    SET_CURRENT_ACTOR_BY_ID: "SET_CURRENT_ACTOR_BY_ID",
    UPDATE_CURRENT_ACTOR_PASSED_TIME: "UPDATE_CURRENT_ACTOR_PASSED_TIME",
    RESET_CURRENT_ACTOR: "RESET_CURRENT_ACTOR",

    SET_PASSED_TIME: "SET_PASSED_TIME"
}
