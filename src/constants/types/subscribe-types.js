export const PUBLIC_PREFIX = "/topic";
export const PRIVATE_PREFIX = "/queue";

export const SubscribeTypes = {
    ADD_ACTOR: PRIVATE_PREFIX + "/addActor",
    REMOVE_ACTOR: PRIVATE_PREFIX + "/removeActor",
    ADD_START: PRIVATE_PREFIX + "/addStart",
    ADD_END: PRIVATE_PREFIX + "/addEnd",
}
