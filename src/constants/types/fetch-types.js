import {DATABASE_URL} from "@/constants/urls";

export const FetchTypes = {
    GET_ACTORS: DATABASE_URL + "/getActors",
    GET_ACCOUNT: DATABASE_URL + "/getAccount",
    CREATE_ACCOUNT: DATABASE_URL + "/createAccount",
}
