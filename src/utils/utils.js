export const createDestination = (subscribeType, subscribeToken) => {
    return subscribeType + "/" + subscribeToken;
}

export const waitFor = conditionFunction => {
    const poll = (resolve, timeout = 100) => {
        if(conditionFunction()) resolve();
        else setTimeout(() => poll(resolve), timeout);
    }
    return new Promise(poll);
}

export const copy = (inputElementId) => {
    /* Get the text field */
    const link = document.getElementById(inputElementId);

    /* Select the text field */
    link.select();
    link.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    document.execCommand("copy");
}

export const displayAsTime = millis => {
    millis = Math.max(0, millis);

    let secs = Math.floor(millis / 1000);

    let mins = Math.floor(secs / 60);
    secs = secs - mins * 60;

    let hours = Math.floor(mins / 60);
    mins = mins - hours * 60;

    //console.log("secs = "+secs)
    //const partialSecs = millis % 1000;
    //console.log("partialSecs = "+partialSecs)

    const secsString = secs < 10 ? "0"+secs : secs;
    const minsString = mins < 10 ? "0"+mins : mins;
    const hoursString = hours < 10 ? "0"+hours : hours;

    return hoursString + ":" + minsString + ":" + secsString;
}

export const open = url => {
    window.open(url, "_self");
}

export const reload = () => {
    location.reload();
}

export const openInNewTab = url => {
    window.open(url);
}
