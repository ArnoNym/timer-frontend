export default class Actor {
    constructor(id, name, acts) {
        this.id = id;
        this.name = name;

        this.passedTime = 0; /* Total time passed */
        this.resolvedPassedTime = 0; /* Time passed of acts that are already finished */
        this.runningPassedTime = 0; /* Time passed of the current act */
        this.lastPassedTime = 0; /* Time of the last act, current or already ended */

        if (acts === null) {
            this.acts = [];
        } else {
            this.acts = acts;
            this.updatePassedTime();
        }

    }

    lastAct() {
        if (this.acts.length === 0) return null;
        return this.acts[this.acts.length - 1]
    }

    isCurrentActor() {
        const lastAct = this.lastAct();
        if (lastAct == null) return false;
        return lastAct.end == null;
    }

    updatePassedTime() {
        let diff = this.updateResolvedPassedTime();
        diff += this.updateRunningPassedTime();
        return diff;
    }
    updateResolvedPassedTime() {
        let pt = 0;
        for (let act of this.acts) {
            if (act.end !== null) {
                pt += act.end - act.start;
            }
        }

        const diff = pt - this.resolvedPassedTime;
        this.passedTime += diff;
        this.resolvedPassedTime = pt;
        return diff;
    }
    updateRunningPassedTime() {
        const runningAct = this.acts[this.acts.length - 1];

        if (runningAct == null) {
            return 0;
        }

        let pt;
        if (runningAct.end == null) {
            const d = new Date();
            pt = d.getTime() - runningAct.start;
            this.lastPassedTime = pt;
        } else {
            pt = 0;
            this.lastPassedTime = this.lastAct().end - this.lastAct().start;
        }

        console.log("updateRunningPassedTime ::: pt="+pt)
        console.log("updateRunningPassedTime ::: this.runningPassedTime="+this.runningPassedTime)

        const diff = pt - this.runningPassedTime;
        this.passedTime += diff;
        this.runningPassedTime = pt;
        console.log("updateRunningPassedTime ::: diff="+diff)
        return diff;
    }
}
