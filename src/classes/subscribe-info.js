export default class SubscribeInfo {
    constructor(subscribeType, messageHandler) {
        this.type = subscribeType;
        this.messageHandler = messageHandler;
    }
}
