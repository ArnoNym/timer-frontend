export default class Actor {
    constructor(id, start, end) {
        this.id = id;
        this.start = start;
        this.end = end;
    }
}
