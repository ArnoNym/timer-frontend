import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/acc/:accountId?',
    alias: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/info/:accountId?',
    name: 'Info',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Info.vue')
  }
]

const router = new VueRouter({
  mode: 'hash', // Default was 'history' but then, when deployed, every link except the home-Link lead to an 404
  base: process.env.BASE_URL,
  routes
})

export default router
